# Install Julia 1.5 by defining package in configure.nix

copy julia_1_5 to `/etc/nixos/pkgs/` (if not exist, `mkdir /etc/nixos/pkgs`)

and in `configure.nix`, modify the `environment.systemPackages` as
```
environment.systemPackages = [
    ...
    ...
    (with pkgs; callPackage ./pkgs/julia_1_5 { })
];
```
